package com.sber.Lecture23_ORM.repository;

import com.sber.Lecture23_ORM.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface TransferRepository extends JpaRepository<Transfer, Long> {

    @Query(
            value = """
                    SELECT * FROM transfers t 
                    INNER JOIN clients c ON t.client_id = c.id
                    INNER JOIN passports p ON c.id = p.client_id
                    WHERE p.number = ?1
                    """,
            nativeQuery = true)
    List<Transfer> findTransfersByPassportNumber(String number);
}
