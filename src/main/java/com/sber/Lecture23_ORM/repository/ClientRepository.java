package com.sber.Lecture23_ORM.repository;

import com.sber.Lecture23_ORM.entity.Client;
import com.sber.Lecture23_ORM.entity.Passport;
import com.sber.Lecture23_ORM.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findClientByPassportNumber(String number);


}
