package com.sber.Lecture23_ORM.entity;


import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"client"})
@Builder
@Entity
@Table(name = "passports")
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "number")
    private String number;

    @OneToOne
    @JoinColumn(name = "client_id")
    private Client client;

    public void setClient(Client client) {
        client.setPassport(this);
        this.client = client;
    }
}
