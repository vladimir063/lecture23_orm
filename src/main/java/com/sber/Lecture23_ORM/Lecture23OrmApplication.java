package com.sber.Lecture23_ORM;

import com.sber.Lecture23_ORM.dao.DaoImpl;
import com.sber.Lecture23_ORM.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class Lecture23OrmApplication {


	public static void main(String[] args) {
		SpringApplication.run(Lecture23OrmApplication.class, args);

	}

}
