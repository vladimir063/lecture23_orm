package com.sber.Lecture23_ORM.dao;

import com.sber.Lecture23_ORM.entity.Client;
import com.sber.Lecture23_ORM.entity.Transfer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@RequiredArgsConstructor
@Repository
public class DaoImpl implements Dao {

    private final EntityManager entityManager;

    @Override
    public Client findClientByPassportNumber(String number) {
        return entityManager.createQuery("""
                        SELECT c FROM Client c
                        INNER JOIN c.passport p
                        WHERE p.number = ?1
                        """, Client.class)
                .setParameter(1, number)
                .getSingleResult();
    }

    @Override
    public List<Transfer> findTransfersByPassportNumber(String number) {
        return entityManager.createNativeQuery("""
                        SELECT * FROM transfers t 
                        INNER JOIN clients c ON t.client_id = c.id
                        INNER JOIN passports p ON c.id = p.client_id
                        WHERE p.number = ?1
                        """, Transfer.class)
                .setParameter(1, number)
                .getResultList();
    }
}
