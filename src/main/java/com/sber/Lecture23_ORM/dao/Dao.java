package com.sber.Lecture23_ORM.dao;

import com.sber.Lecture23_ORM.entity.Client;
import com.sber.Lecture23_ORM.entity.Transfer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Dao {

    Client findClientByPassportNumber(String number);

    List<Transfer> findTransfersByPassportNumber(String number);


}
