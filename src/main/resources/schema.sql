DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS passports;
DROP TABLE IF EXISTS transfers;


CREATE TABLE transfers
(
    id   BIGINT AUTO_INCREMENT PRIMARY KEY,
    amount numeric NOT NULL,
    client_id BIGINT
);

CREATE TABLE clients
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    age INT NOT NULL
);

CREATE TABLE passports
(
    id   BIGINT AUTO_INCREMENT PRIMARY KEY,
    number varchar(255) NOT NULL,
    client_id BIGINT NOT NULL UNIQUE REFERENCES clients (id)
);
