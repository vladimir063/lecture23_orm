package com.sber.Lecture23_ORM.repository;

import com.sber.Lecture23_ORM.entity.Client;
import com.sber.Lecture23_ORM.entity.Passport;
import com.sber.Lecture23_ORM.entity.Transfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class RepositoryTest {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    TransferRepository transferRepository;


    @Test
    void findClientByPassportNumber() {
        Client client = clientRepository.findClientByPassportNumber("84861223")
                .orElseThrow(RuntimeException::new);

        assertThat(client.getName()).isEqualTo("Ivan");
    }

    @Test
    void findTransfersByPassportNumber() {
        List<Transfer> transfers = transferRepository.findTransfersByPassportNumber("84861223");
        assertThat(transfers.get(0).getAmount()).isEqualTo(521);
    }
}