package com.sber.Lecture23_ORM.dao;

import com.sber.Lecture23_ORM.entity.Client;
import com.sber.Lecture23_ORM.entity.Passport;
import com.sber.Lecture23_ORM.entity.Transfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class DaoTest {

    @Autowired
    DaoImpl daoImpl;

    @Test
    void findClientByPassportNumber() {
        Client client = daoImpl.findClientByPassportNumber("84861223");
        assertThat(client.getName()).isEqualTo("Ivan");
    }

    @Test
    void findTransfersByPassportNumber() {
        List<Transfer> transfers = daoImpl.findTransfersByPassportNumber("84861223");
        assertThat(transfers.get(0).getAmount()).isEqualTo(521);
    }
}